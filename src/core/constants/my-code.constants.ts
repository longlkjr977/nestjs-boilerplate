const myCode = {
  _UNKNOWN: {
    numberCode: 0,
    stringCode: 'UNKNOWN',
    message: 'UNKNOWN',
  },
  _SUCCESSFUL: {
    numberCode: 1,
    stringCode: 'SUCCESSFUL',
    message: 'request successful',
  },
  _FAIL: {
    numberCode: -1,
    stringCode: 'FAIL',
    message: 'request fail',
  },
  _USER_NOT_FOUND: {
    numberCode: 2,
    stringCode: 'USER_NOT_FOUND',
    message: 'user not found',
  },
  _PASSWORD_NOT_CORRCT: {
    numberCode: 3,
    stringCode: 'PASSWORD_NOT_CORRCT',
    message: 'password not correct',
  },
  _USER_IS_BLOCKED: {
    numberCode: 3,
    stringCode: 'USER_IS_BLOCKED',
    message: 'user is blocked',
  },
  _UNAUTHORIZED: {
    numberCode: 3,
    stringCode: 'UNAUTHORIZED',
    message: 'unauthorized',
  },
  _TOKEN_INVALID: {
    numberCode: 3,
    stringCode: 'TOKEN_INVALID',
    message: 'token invalid',
  },
  _JWT_EXPIRED: {
    numberCode: 3,
    stringCode: 'JWT_EXPIRED',
    message: 'jwt expired',
  },
  _FORBIDDEN: {
    numberCode: 3,
    stringCode: 'FORBIDDEN',
    message: 'forbidden',
  },
  _PERMISSION_SET_TO_META_DATA_INVALID: {
    numberCode: 3,
    stringCode: 'PERMISSION_SET_TO_META_DATA_INVALID',
    message: 'permissions set to metadata invalid',
  },
  _REQUEST_INVALID: {
    numberCode: 3,
    stringCode: 'REQUEST_INVALID',
    message: 'request invalid',
  },
};

export default myCode;
