export interface MyResponseCode {
  numberCode?: number;
  stringCode?: string;
  message?: string;
}
