# NESTJS BOILERPALTE

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://www.facebook.com/Talonjr.7/)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Description

Sample project Nestjs

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run dev
```

## deploy with pm2

```bash
# deploy
$ pm2 start dist/main.js --name pm2
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Swagger
```bash
# link swagger
http://localhost:8864/document
```

## Compodoc - https://compodoc.app/
```bash
# Generate project documentation using the following command
$ npx @compodoc/compodoc -p tsconfig.json -s
```

## Stay in touch

- Author - Tiểu Bạch Long
- Facebook - [https://www.facebook.com/Talonjr.7/](https://www.facebook.com/Talonjr.7/)

## License

Nest is [MIT licensed](LICENSE).
